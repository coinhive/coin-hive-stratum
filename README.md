# coin-hive-stratum

CoinHive Stratum Proxy
This proxy allows you to use CoinHive&#39;s JavaScript miner on a custom stratum pool.

You can mine cryptocurrencies Monero (XMR) and Electroneum (ETN).